/*
 *  eFRen (v1.0 Beta) - eFile Renamer
    Copyright (C) 2009 Aryo Pinandito / A-Works, Inc.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace eFRen
{
    public partial class eFRen : Form
    {
        private String folderPath;
        private String[] files;
        private String[] fileNames;

        public eFRen()
        {
            InitializeComponent();
        }

        private void btBrowse_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                getFileList(tbFilterSource.Text);
            }
        }

        private void getFileList(String filter)
        {
            if (!cbRegexSource.Checked)
            {
                filter = "*" + filter + "*";
            }
            try
            {
                folderPath = folderBrowserDialog.SelectedPath;
                tbPath.Text = folderPath;
                if (cbRegexSource.Checked) files = Directory.GetFiles(folderPath);
                else files = Directory.GetFiles(folderPath, filter);
                fileNames = new String[files.Length];
                int i = 0;
                foreach (String file in files)
                {
                    fileNames[i] = file.Substring(file.LastIndexOf('\\') + 1);
                    i++;
                }
                listSource.Items.Clear();
                if (!cbRegexSource.Checked)
                {
                    listSource.Items.AddRange(fileNames);
                }
                else
                {
                    foreach (String fileName in fileNames)
                    {
                        Regex r = new Regex(@tbFilterSource.Text, RegexOptions.IgnoreCase);
                        if (r.IsMatch(fileName))
                        {
                            listSource.Items.Add(fileName);
                        }
                    }
                }
                Replace();
            }
            catch
            {

            }
        }

        private void tbFilterSource_TextChanged(object sender, EventArgs e)
        {
            getFileList(tbFilterSource.Text);
        }

        private void Replace()
        {
            if (tbFilterSource.Text.Length > 0)
            {
                listRename.Items.Clear();
                foreach (String fileName in fileNames)
                {
                    if (!cbRegexSource.Checked)
                    {
                        listRename.Items.Add(fileName.Replace(tbFilterSource.Text, tbFilterRename.Text));
                    }
                    else
                    {
                        Regex r = new Regex(@tbFilterSource.Text, RegexOptions.IgnoreCase);
                        if (r.IsMatch(fileName))
                        {
                            listRename.Items.Add(Regex.Replace(fileName, @tbFilterSource.Text, tbFilterRename.Text, RegexOptions.IgnoreCase));
                        }
                    }
                }
            }
        }

        private void tbFilterRename_TextChanged(object sender, EventArgs e)
        {
            Replace();
        }

        private void cbRegexSource_CheckedChanged(object sender, EventArgs e)
        {
            getFileList(tbFilterSource.Text);
        }

        private void linkHelp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Help help = new Help();
            help.ShowDialog();
        }

        private void Rename()
        {
            if (listSource.Items.Count == listRename.Items.Count && tbFilterSource.Text.Length > 0 && listSource.Items.Count > 0 && listRename.Items.Count > 0)
            {
                if (folderPath[folderPath.Length - 1] != '\\') folderPath = folderPath + "\\";
                int i = 0;
                int j = listRename.Items.Count;
                int inc = (int)((i + 1)*100 / j);
                foreach (Object file in listRename.Items)
                {
                    String fileSource = folderPath + listSource.Items[i].ToString();
                    String fileRename = folderPath + listRename.Items[i].ToString();
                    try
                    {
                        File.Move(@fileSource, @fileRename);
                    }
                    catch (Exception ex)
                    {
                        DialogResult dr = MessageBox.Show("Error renaming file: " + listSource.Items[i].ToString() + "\n" + ex.Message + "Continue?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (dr == DialogResult.No) break;
                    }
                    progressBar.Increment(inc);
                    i++;
                }
                progressBar.Value = 100;
                MessageBox.Show("Rename process completed", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                progressBar.Value = 0;
                tbFilterSource.Text = "";
                tbFilterRename.Text = "";
                getFileList(tbFilterSource.Text);
            }
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btApply_Click(object sender, EventArgs e)
        {
            if (listSource.Items.Count == listRename.Items.Count && tbFilterSource.Text.Length > 0 && listSource.Items.Count > 0 && listRename.Items.Count > 0)
            {
                if (DialogResult.Yes == MessageBox.Show("Start renaming process?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    Rename();
                }
            }
            else
            {
                MessageBox.Show("Notice: Nothing to search", "Notice", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Help help = new Help();
            help.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }


    }
}