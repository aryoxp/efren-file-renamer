namespace eFRen
{
    partial class eFRen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(eFRen));
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.gbFolder = new System.Windows.Forms.GroupBox();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.btBrowse = new System.Windows.Forms.Button();
            this.listSource = new System.Windows.Forms.ListBox();
            this.listRename = new System.Windows.Forms.ListBox();
            this.tbFilterSource = new System.Windows.Forms.TextBox();
            this.cbRegexSource = new System.Windows.Forms.CheckBox();
            this.tbFilterRename = new System.Windows.Forms.TextBox();
            this.btExit = new System.Windows.Forms.Button();
            this.btApply = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.linkHelp = new System.Windows.Forms.LinkLabel();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.gbProgress = new System.Windows.Forms.GroupBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conMenuFile = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conMenuHelp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbFolder.SuspendLayout();
            this.gbProgress.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.conMenuFile.SuspendLayout();
            this.conMenuHelp.SuspendLayout();
            this.SuspendLayout();
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.Description = "Please select a folder containing file(s) to be renamed.";
            this.folderBrowserDialog.ShowNewFolderButton = false;
            // 
            // gbFolder
            // 
            this.gbFolder.Controls.Add(this.tbPath);
            this.gbFolder.Controls.Add(this.btBrowse);
            this.gbFolder.Location = new System.Drawing.Point(12, 27);
            this.gbFolder.Name = "gbFolder";
            this.gbFolder.Size = new System.Drawing.Size(439, 49);
            this.gbFolder.TabIndex = 1;
            this.gbFolder.TabStop = false;
            this.gbFolder.Text = "Directory Path";
            // 
            // tbPath
            // 
            this.tbPath.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbPath.Location = new System.Drawing.Point(6, 21);
            this.tbPath.Name = "tbPath";
            this.tbPath.ReadOnly = true;
            this.tbPath.Size = new System.Drawing.Size(345, 21);
            this.tbPath.TabIndex = 2;
            // 
            // btBrowse
            // 
            this.btBrowse.Location = new System.Drawing.Point(358, 20);
            this.btBrowse.Name = "btBrowse";
            this.btBrowse.Size = new System.Drawing.Size(75, 23);
            this.btBrowse.TabIndex = 1;
            this.btBrowse.Text = "Browse";
            this.btBrowse.UseVisualStyleBackColor = true;
            this.btBrowse.Click += new System.EventHandler(this.btBrowse_Click);
            // 
            // listSource
            // 
            this.listSource.FormattingEnabled = true;
            this.listSource.Location = new System.Drawing.Point(18, 141);
            this.listSource.Name = "listSource";
            this.listSource.Size = new System.Drawing.Size(210, 212);
            this.listSource.TabIndex = 2;
            // 
            // listRename
            // 
            this.listRename.FormattingEnabled = true;
            this.listRename.Location = new System.Drawing.Point(234, 141);
            this.listRename.Name = "listRename";
            this.listRename.Size = new System.Drawing.Size(210, 212);
            this.listRename.TabIndex = 3;
            // 
            // tbFilterSource
            // 
            this.tbFilterSource.Location = new System.Drawing.Point(18, 109);
            this.tbFilterSource.Name = "tbFilterSource";
            this.tbFilterSource.Size = new System.Drawing.Size(210, 21);
            this.tbFilterSource.TabIndex = 4;
            this.tbFilterSource.TextChanged += new System.EventHandler(this.tbFilterSource_TextChanged);
            // 
            // cbRegexSource
            // 
            this.cbRegexSource.AutoSize = true;
            this.cbRegexSource.Location = new System.Drawing.Point(67, 87);
            this.cbRegexSource.Name = "cbRegexSource";
            this.cbRegexSource.Size = new System.Drawing.Size(78, 17);
            this.cbRegexSource.TabIndex = 5;
            this.cbRegexSource.Text = "Use Regex";
            this.cbRegexSource.UseVisualStyleBackColor = true;
            this.cbRegexSource.CheckedChanged += new System.EventHandler(this.cbRegexSource_CheckedChanged);
            // 
            // tbFilterRename
            // 
            this.tbFilterRename.Location = new System.Drawing.Point(234, 109);
            this.tbFilterRename.Name = "tbFilterRename";
            this.tbFilterRename.Size = new System.Drawing.Size(210, 21);
            this.tbFilterRename.TabIndex = 6;
            this.tbFilterRename.TextChanged += new System.EventHandler(this.tbFilterRename_TextChanged);
            // 
            // btExit
            // 
            this.btExit.Location = new System.Drawing.Point(369, 372);
            this.btExit.Name = "btExit";
            this.btExit.Size = new System.Drawing.Size(75, 23);
            this.btExit.TabIndex = 8;
            this.btExit.Text = "Exit";
            this.btExit.UseVisualStyleBackColor = true;
            this.btExit.Click += new System.EventHandler(this.btExit_Click);
            // 
            // btApply
            // 
            this.btApply.Location = new System.Drawing.Point(288, 372);
            this.btApply.Name = "btApply";
            this.btApply.Size = new System.Drawing.Size(75, 23);
            this.btApply.TabIndex = 9;
            this.btApply.Text = "Apply";
            this.btApply.UseVisualStyleBackColor = true;
            this.btApply.Click += new System.EventHandler(this.btApply_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Search";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(231, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Replace";
            // 
            // linkHelp
            // 
            this.linkHelp.AutoSize = true;
            this.linkHelp.Location = new System.Drawing.Point(142, 88);
            this.linkHelp.Name = "linkHelp";
            this.linkHelp.Size = new System.Drawing.Size(28, 13);
            this.linkHelp.TabIndex = 12;
            this.linkHelp.TabStop = true;
            this.linkHelp.Text = "Help";
            this.linkHelp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkHelp_LinkClicked);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(10, 20);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(243, 13);
            this.progressBar.TabIndex = 13;
            // 
            // gbProgress
            // 
            this.gbProgress.Controls.Add(this.progressBar);
            this.gbProgress.Location = new System.Drawing.Point(12, 359);
            this.gbProgress.Name = "gbProgress";
            this.gbProgress.Size = new System.Drawing.Size(270, 43);
            this.gbProgress.TabIndex = 14;
            this.gbProgress.TabStop = false;
            this.gbProgress.Text = "Progress";
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(463, 24);
            this.menuStrip.TabIndex = 15;
            this.menuStrip.Text = "MainMenu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDown = this.conMenuFile;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDown = this.conMenuHelp;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // conMenuFile
            // 
            this.conMenuFile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.conMenuFile.Name = "conMenuFile";
            this.conMenuFile.ShowImageMargin = false;
            this.conMenuFile.Size = new System.Drawing.Size(79, 26);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(78, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // conMenuHelp
            // 
            this.conMenuHelp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.aboutToolStripMenuItem});
            this.conMenuHelp.Name = "conMenuFile";
            this.conMenuHelp.ShowImageMargin = false;
            this.conMenuHelp.Size = new System.Drawing.Size(158, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem1.Text = "Regular Expressions";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // eFRen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 415);
            this.Controls.Add(this.gbProgress);
            this.Controls.Add(this.linkHelp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btApply);
            this.Controls.Add(this.btExit);
            this.Controls.Add(this.tbFilterRename);
            this.Controls.Add(this.cbRegexSource);
            this.Controls.Add(this.tbFilterSource);
            this.Controls.Add(this.listRename);
            this.Controls.Add(this.listSource);
            this.Controls.Add(this.gbFolder);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "eFRen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "eFRen";
            this.gbFolder.ResumeLayout(false);
            this.gbFolder.PerformLayout();
            this.gbProgress.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.conMenuFile.ResumeLayout(false);
            this.conMenuHelp.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.GroupBox gbFolder;
        private System.Windows.Forms.Button btBrowse;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.ListBox listSource;
        private System.Windows.Forms.ListBox listRename;
        private System.Windows.Forms.TextBox tbFilterSource;
        private System.Windows.Forms.CheckBox cbRegexSource;
        private System.Windows.Forms.TextBox tbFilterRename;
        private System.Windows.Forms.Button btExit;
        private System.Windows.Forms.Button btApply;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel linkHelp;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.GroupBox gbProgress;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip conMenuFile;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip conMenuHelp;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

